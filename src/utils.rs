use std::collections::HashMap;

pub fn find_max(count_map: &HashMap<isize, usize>) -> Vec<(isize, usize)> {
    let mut max_diff: Option<usize> = None;
    let mut maximums = vec![];
    for (diff, count) in count_map {
        if let Some(current_max_count) = &max_diff {
            if current_max_count < count {
                max_diff = Some(*count);
                maximums = vec![];
                maximums.push((*diff, *count));
            } else if current_max_count == count {
                maximums.push((*diff, *count));
            }
        } else {
            max_diff = Some(*count)
        }
    }

    maximums
}


pub fn add_count(value: &isize, count_map: &mut HashMap<isize, usize>) {
    if let Some(count) = count_map.get_mut(value) {
        *count += 1
    } else {
        count_map.insert(*value, 1);
    }
}
