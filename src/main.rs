use std::collections::{HashMap, HashSet};
use std::convert::TryInto;

use num_bigint::{BigUint, ToBigUint};
use num_traits::cast::ToPrimitive;

use crate::eye::EyeMessage;
use crate::trigram::TrigramMessage;
use crate::utils::{add_count, find_max};

mod eye;
mod messages;
mod trigram;
mod utils;

fn main() -> Result<(), String> {
    let mut eyes_map: HashMap<&str, EyeMessage> = HashMap::new();
    eyes_map.insert("east1", messages::EAST1.parse()?);
    eyes_map.insert("west1", messages::WEST1.parse()?);
    eyes_map.insert("east2", messages::EAST2.parse()?);
    eyes_map.insert("west2", messages::WEST2.parse()?);
    eyes_map.insert("east3", messages::EAST3.parse()?);
    eyes_map.insert("west3", messages::WEST3.parse()?);
    eyes_map.insert("east4", messages::EAST4.parse()?);
    eyes_map.insert("west4", messages::WEST4.parse()?);
    eyes_map.insert("east5", messages::EAST5.parse()?);

    let trigram_map: HashMap<&str, TrigramMessage> = eyes_map.clone().into_iter()
        .map(|(key, eye)| -> (&str, TrigramMessage){ (key, eye.try_into().unwrap()) })
        .collect();

    let base10_map: HashMap<&str, Vec<u8>> = trigram_map.clone().into_iter()
        .map(|(key, trigram)| -> (&str, Vec<u8>){ (key, trigram.into()) })
        .collect();

    for (world, data) in &base10_map {
        let mut missing_nums = vec![];
        let data_len = data.len();
        for i in 0..83 {
            let freq = data.iter()
                .filter(|val| **val == i)
                .count();

            if freq == 0 {
                missing_nums.push(i);
            }
        }
        eprintln!(
            "missing_nums for message {} = {:?} (total = {}/83, message len = {}, avg space = {})",
            world, missing_nums, missing_nums.len(), data_len, 83 / missing_nums.len()
        );
    }

    let mut all_diffs: Vec<isize> = (0..83).into_iter().collect();

    // Check diffs between p and p+1
    for (world, data) in &base10_map {
        let mut diffs = vec![];
        let mut iter = data.iter().peekable();
        while let Some(num) = iter.next() {
            if let Some(next) = iter.peek() {
                let diff: isize = (**next as isize - *num as isize + 83) % 83;
                diffs.push(diff);
            }
        }

        let mut counts: HashMap<isize, usize> = HashMap::new();
        for diff in diffs {
            if let Some(value) = counts.get_mut(&diff) {
                *value = *value + 1;
            } else {
                counts.insert(diff, 1);
            }
        }

        let maximums = find_max(&counts);

        eprintln!("Finding diff maximum for {:?} = {:?}", world, maximums);
        for i in 0..83 {
            if let None = counts.get(&i) {
                all_diffs = all_diffs.into_iter()
                    .filter(|value| value != &i)
                    .collect();
            }
        }
        eprintln!("   - count for diff 22 = {:?}", counts.get(&22));
        eprintln!("   - count for diff 53 = {:?}", counts.get(&53));
        eprintln!("   - count for diff 62 = {:?}", counts.get(&62));

        eprintln!("   - counts.len() = {:?}", counts.len());
    }

    eprintln!("all_diffs = {:?}", all_diffs);


    // Test if last is same char in e2/w2
    for b in 0..1000 {
        if (16 * b) % 83 == 18 {
            eprintln!("b = {:?}", b);
        }
    }

    // Check diffs between same pos
    let mut max_iter = 0;
    let mut a_similarities = HashMap::new();
    for (world1, data1) in &base10_map {
        for (world2, data2) in &base10_map {
            if world1 != world2 {
                let mut iter1 = data1.iter();
                let mut iter2 = data2.iter();

                while let Some(next1) = iter1.next() {
                    if let Some(next2) = iter2.next() {
                        let diff = (*next2 as isize - *next1 as isize + 83) % 83;
                        if let Some(sim) = a_similarities.get_mut(&diff) {
                            *sim += 1
                        } else {
                            a_similarities.insert(diff, 1);
                        }
                        max_iter += 1;
                    } else { break; }
                }
            }
        }
    }

    let maximums = find_max(&a_similarities);
    eprintln!("Finding diff similarities between worlds {:?}", maximums);

    let mut a_sim_vec: Vec<_> = a_similarities.iter().collect();
    a_sim_vec.sort_by(|(_, val1), (_, val2)| val2.cmp(val1));
    eprintln!("a_sim_vec = {:?}", a_sim_vec);
    eprintln!("(max_iter, a_sim_vec.len()) = {:?}", (max_iter, a_sim_vec.len()));

    a_sim_vec.sort_by(|(val1, _), (val2, _)| val2.cmp(val1));
    eprintln!("a_sim_vec = {:?}", a_sim_vec);
    eprintln!("a_sim_vec.len() = {:?}", a_sim_vec.len());

    // Failed - try to check for matching "c"
    //let mut possible_c = HashMap::new();
    //for (world, data) in &base10_map {
    //    for (pos, value) in data.iter().enumerate() {
    //        for n in 0..150 {
    //            for c in 0..83 {
    //                let q = *value as isize + 83 * n as isize - 53 * pos as isize - c as isize;
    //                let x = q / 62;
    //                if q % 62 == 0 && x >= 0 && x < 83 {
    //                    add_count(&c, &mut possible_c);

    //                    //eprintln!("{} (value, pos, n, c, x) = {:?}", world, (value, pos, n, c, x));
    //                }
    //            }
    //        }
    //    }
    //}

    //let mut possible_c_vec: Vec<_> = possible_c.iter().collect();
    //possible_c_vec.sort_by(|(_, val1), (_, val2)| val2.cmp(val1));

    //eprintln!("possible_c = {:?}", possible_c_vec);
    //eprintln!("possible_c_vec.len() = {:?}", possible_c_vec.len());

    let all_num: Vec<BigUint> = (0..83).into_iter()
        .map(|v| v.to_biguint().unwrap())
        .collect();
    let big_mod = 83.to_biguint().unwrap();
    //for i in 0..83 {
    //    let big_i = i.to_biguint().unwrap();
    //    let mut all_num_pow: Vec<BigUint> = all_num.iter()
    //        .map(|value| value.modpow(&big_i, &big_mod))
    //        .collect();
    //    //all_num_pow.sort();
    //    //all_num_pow.dedup();
//
    //    //if all_num_pow.len() == 83 {
    //    //eprintln!("(i, all_num_pow) = {:?}", (i, all_num_pow));
    //    //}
    //}

    //for (world, data) in &base10_map {
    //    //let i = 52;
    //    for i in 1..82 {
    //        let big_i = i.to_biguint().unwrap();
    //        let mut transformed_data: Vec<BigUint> = data.iter()
    //            .map(|value| value.to_biguint().unwrap().modpow(&big_i, &big_mod))
    //            .collect();
//
    //        //eprintln!("transformed_data = {:?}", transformed_data);
    //        transformed_data.sort();
    //        transformed_data.dedup();
    //        let max = transformed_data.last().unwrap().to_u64().unwrap();
//
    //        if max < 80 {
    //            eprintln!("data^{} for {} = {:?}, with max {}", i, world, transformed_data.len(), max);
    //        }
    //    }
    //}

    let mut possible_diff_values = HashMap::new();
    let worlds: Vec<_> = base10_map.iter().map(|(w, _)| w).collect();

    for (j, world1) in worlds.iter().enumerate() {
        for j2 in j + 1..worlds.len() {
            let world2 = worlds.get(j2).unwrap();
            let data1 = base10_map.get(**world1).unwrap();
            let data2 = base10_map.get(**world2).unwrap();

            for i in 0..1 {
                let diff = (data2[i] as i32 - data1[i] as i32).abs();
                if let Some(value) = possible_diff_values.get_mut(&diff) {
                    eprintln!("(world2, world1, diff) = {:?}", (world2, world1, diff));
                    *value = *value + 1;
                } else {
                    possible_diff_values.insert(diff, 1);
                }
            }
        }
    }


    let mut possible_diff_values_vec: Vec<_> = possible_diff_values.iter().collect();

    possible_diff_values_vec.sort_by(|(k1, _), (k2, _)| k1.cmp(k2));
    eprintln!("possible_diff_values_vec = {:?}", possible_diff_values_vec);
    eprintln!("possible_diff_values_vec.len() = {:?}", possible_diff_values_vec.len());

    possible_diff_values_vec.sort_by(|(_, v1), (_, v2)| v1.cmp(v2));
    eprintln!("possible_diff_values_vec = {:?}", possible_diff_values_vec);
    eprintln!("possible_diff_values_vec.len() = {:?}", possible_diff_values_vec.len());

    let mut tops = HashMap::new();
    for (world, data) in &base10_map {
        let mut diff_counts = HashMap::new();
        for i in 0..data.len() {
            for j in i + 1..data.len() {
                let diff = (data[j] as i32 - data[i] as i32 + 83) % 83;
                let key = (j - i, diff);

                //if key == (4, 0) {
                //    eprintln!("(i, world, data[i], data[j]) = {:?}", (i, world, data[i], data[j]));
                //}

                if let Some(value) = diff_counts.get_mut(&key) {
                    *value = *value + 1;
                } else {
                    diff_counts.insert(key, 1);
                }
            }
        }

        let mut diff_counts_vec: Vec<_> = diff_counts.iter().collect();
        diff_counts_vec.sort_by(|(_, v1), (_, v2)| v2.cmp(v1));
        let diff_counts_vec_max: Vec<_> = diff_counts_vec.iter().collect();

        for ((top_diff_key, top_diff_val), top_diff_count) in &diff_counts_vec_max {
            let key: (usize, i32) = (*top_diff_key, *top_diff_val);
            if let Some(value) = tops.get_mut(&key) {
                *value = *value + **top_diff_count;
            } else {
                tops.insert(key, **top_diff_count);
            }
        }

        eprintln!("(world, diff_counts.len()) = {:?}", (world, diff_counts.len()));
    }

    let mut tops_vec: Vec<_> = tops.iter().collect();

    tops_vec.sort_by(|(_, v1), (_, v2)| v2.cmp(v1));
    let top_tops_vec: Vec<_> = tops_vec.iter()
        // USE THIS FILTER TO LOOK AT ANY DIFF
        .filter(|((dist, diff), count)| *diff == 0 && **count >= 1)
        .collect();
    for ((dist, val), count) in &top_tops_vec {
        //eprintln!("-> Diff of {} with position shift of {} is repeated {} times", val, dist, count);
    }

    let mut top_diff: Vec<_> = top_tops_vec.iter()
        .map(|((dist, diff), count)| diff)
        .collect();

    top_diff.sort();
    top_diff.dedup();

    eprintln!("top_diff = {:?}", top_diff);
    eprintln!("top_diff.len() = {:?}", top_diff.len());

    let mut top_dist: Vec<_> = top_tops_vec.iter()
        .map(|((dist, diff), count)| dist)
        .collect();

    top_dist.sort();
    top_dist.dedup();

    eprintln!("top_dist = {:?}", top_dist);
    eprintln!("top_dist.len() = {:?}", top_dist.len());

    /*
    for (world, data) in &base10_map {
        let mut message0 = vec![];
        let mut message1 = vec![];
        let mut message2 = vec![];
        let mut message3 = vec![];
        for i in 0..data.len() {
            match i%4 {
                0 => message0.push(data[i]),
                1 => message1.push(data[i]),
                2 => message2.push(data[i]),
                3 => message3.push(data[i]),
                _ => ()
            }
        }

        eprintln!("message0 for {} = {:?}", world, message0);
        eprintln!("message1 for {} = {:?}", world, message1);
        eprintln!("message2 for {} = {:?}", world, message2);
        eprintln!("message3 for {} = {:?}", world, message3);
    }
    */

    for (world, data) in &base10_map {
        eprintln!("data for {} = {:?}", world, data);
    }

    /*
    for (world, data) in &base10_map {
        let test: Vec<&str> = data.iter()
            //.map(|value| value % 53)
            .map(|val| match val {
                1 => "a",
                2 => "a",
                3 => "a",
                4 => "b",
                5 => "b",
                6 => "b",
                7 => "b",
                8 => "b",
                9 => "c",
                10 => "c",
                11=> "c",
                12 => "c",
                13=> "c",
                14=> "c",
                15=> "c",
                16=> "d",
                17=> "d",
                18=> "d",
                19=> "d",
                20=> "d",
                /*
                //27 => "A",
                //33 => "B",
                //34 => "C",
                //36 => "D",
                //50 => "E",
                //63 => "F",
                76 => "Y",
                //77 => "H",
                //80 => "I",

                66 => " ",
                5 => " ",
                49 => "I",
                48 => "T",
                62 => "H",
                13 => "E",

                75 => " ",

                54 => "A",
                2 => "M",
                60 => " ",

                31 => "H",
                59 => " ",

                69 => "g",
                23 => "i",

                74 => "k",
                46 => "m",

                29 => "n",
                32 => "o",
                11 => "p",

                1 => "q",
                40 => "r",

                42 => "s",

                48 => "D",
                49 => "D",

                75 => "E",
                62 => "E",

                54 => "F",
                13 => "F",

                69 => "G",
                2 => "G",
                23 => "G",
                //11 => "-", // down

                74 => "H",
                60 => "H",
                46 => "H",
                //40 => "-", // down

                29 => "I",
                32 => "I",
                11 => "I",

                1 => "J",
                40 => "J",

                42 => "K",

                49 => "t",
                75 => "h",
                54 => "e",
                2 => "m",
                69 => "s",
                23 => " ",

                60 => "e",
                29 => " ",
                40 => "i",

                13 => "e",
                54 => "2",

                79 => "3",
                48 => "t",
                62 => "h",
                61 => "s",
                42 => " ",

                29 => "5",

                24 => "i",
                30 => "7",

                70 => "t",

                 */

                _ => "_"
            })
            .collect();

        let mut full = String::new();
        for str in test {
            full.push_str(str);
        }

        eprintln!("full {} = {:?}", world, full);
    }
    */

    // Find most common pairs
    /*
    let mut pairs_vec = vec![];
    for (world, data) in &base10_map {
        let mut iter = data.iter().peekable();
        while let Some(num) = iter.next() {
            if let Some(next) = iter.peek() {
                pairs_vec.push((*num, **next))
            }
        }
    }
    
    let mut pairs_count = HashMap::new();
    for pair in pairs_vec {
        if let Some(count) = pairs_count.get_mut(&pair) {
            *count += 1;
        } else {
            pairs_count.insert(pair.clone(), 1);
        }
    }

    let mut pairs_list_with_count: Vec<_> = pairs_count.iter().collect();
    pairs_list_with_count.sort_by(|(_, count1),(_, count2)| count2.cmp(count1));
    let pairs_list_with_count_filtered: Vec<_> = pairs_list_with_count.iter()
        .filter(|(_, count)| **count > 1)
        .collect();

    eprintln!("pairs = {:?}", pairs_list_with_count_filtered);
    */

    // Find max repetitions
    let mut repetitions = HashMap::new();
    for key_size in 100..200 {
        let mut repetitions_for_key_size = 0;
        for (world, data) in &base10_map {
            for (i, &value) in data.iter().enumerate() {
                for (other_world, other_data) in &base10_map {
                    for j in i + key_size..other_data.len() {
                        if let Some(&other_value) = other_data.get(j) {
                            if value == other_value {
                                repetitions_for_key_size += 1;
                                //eprintln!("(world, i, other_world, j, value, other_value) = {:?}", (world, i, other_world, j, value, other_value));
                            }
                        }
                    }
                }
            }
        }
        repetitions.insert(key_size as isize, repetitions_for_key_size);
    }

    eprintln!("repetitions = {:?}", repetitions);
    eprintln!("find_max(&repetitions) = {:?}", find_max(&repetitions));

    let mut repetitions_list: Vec<_> = repetitions.iter().collect();
    repetitions_list.sort_by(|(_, count1),(_, count2)| count2.cmp(count1));

    eprintln!("repetitions_list = {:?}", repetitions_list);


    Ok(())
}
