use crate::eye::{Eye, EyeMessage};
use crate::trigram::builder::build_trigrams;
use std::convert::{TryFrom, TryInto};
use std::vec::IntoIter;

pub mod builder;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Trigram(Eye, Eye, Eye);

impl Trigram {
    pub fn has_eyes_in_same_directions(&self) -> bool {
        self.0 == self.1 && self.1 == self.2
    }
}

#[derive(Debug, Clone)]
pub struct TrigramMessage(Vec<Trigram>);

impl From<Trigram> for u8 {
    fn from(trigram: Trigram) -> Self {
        let first_num: u8 = trigram.0.into();
        let second_num: u8 = trigram.1.into();
        let third_num: u8 = trigram.2.into();

        first_num * 25 + second_num * 5 + third_num
    }
}

impl TryFrom<u8> for Trigram {
    type Error = String;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        let third_num = value % 5;
        let second_num = value % 25 - third_num;
        let first_num = value - second_num;

        Ok(Trigram(first_num.try_into()?, second_num.try_into()?, third_num.try_into()?))
    }
}

impl TryFrom<EyeMessage> for TrigramMessage {
    type Error = String;

    fn try_from(eyes: EyeMessage) -> Result<Self, Self::Error> {
        let mut trigrams = vec![];

        let mut eyes_iter = eyes.into_iter();
        while eyes_iter.len() != 0 {
            let top_eye_line = eyes_iter.next().ok_or("Missing top line")?;
            let bottom_eye_line = eyes_iter.next().ok_or("Missing bottom line")?;
            let mut trigram_part = build_trigrams(top_eye_line.into_iter(), bottom_eye_line.into_iter())?;
            trigrams.append(&mut trigram_part);
        }

        Ok(TrigramMessage(trigrams))
    }
}

impl From<TrigramMessage> for Vec<u8> {
    fn from(trigrams: TrigramMessage) -> Self {
        trigrams.0.into_iter()
            .map(|trigram| u8::from(trigram))
            .collect()
    }
}

impl IntoIterator for TrigramMessage {
    type Item = Trigram;
    type IntoIter = IntoIter<Trigram>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}
