use crate::eye::Eye;
use crate::trigram::Trigram;
use std::vec::IntoIter;

pub fn build_trigrams(mut top_line: IntoIter<Eye>, mut bottom_line: IntoIter<Eye>) -> Result<Vec<Trigram>, String> {
    // A flag to alternate between iteration types
    let mut top_iteration = true;
    let mut result = vec![];

    while top_line.len() != 0 || bottom_line.len() != 0 {
        if top_iteration {
            let first_eye = top_line.next().ok_or("Missing top first eye".to_string())?;
            let second_eye = top_line.next().ok_or("Missing top second eye".to_string())?;
            let third_eye = bottom_line.next().ok_or("Missing top third eye".to_string())?;
            result.push(Trigram(first_eye, second_eye, third_eye));
        } else {
            // Get the 2nd eye first
            let second_eye = bottom_line.next().ok_or("Missing bottom second eye".to_string())?;
            let first_eye = bottom_line.next().ok_or("Missing bottom first eye".to_string())?;
            let third_eye = top_line.next().ok_or("Missing bottom third eye".to_string())?;
            //let first_eye = bottom_line.next().ok_or("Missing bottom first eye".to_string())?;
            //let second_eye = top_line.next().ok_or("Missing bottom second eye".to_string())?;
            //let third_eye = bottom_line.next().ok_or("Missing bottom third eye".to_string())?;
            result.push(Trigram(first_eye, second_eye, third_eye));
        }
        top_iteration = !top_iteration;
    }

    Ok(result)
}

#[cfg(test)]
mod tests {
    use crate::eye::Eye;
    use crate::trigram::builder::build_trigrams;

    #[test]
    fn it_builds() {
        let top_eyes_char = vec!['2', '0', '1'];
        let bottom_eyes_char = vec!['0', '3', '2'];

        let top_iter: Result<Vec<Eye>, String> = top_eyes_char
            .into_iter().map(|c| Eye::from_char(c)).collect();
        let bottom_iter: Result<Vec<Eye>, String> = bottom_eyes_char
            .into_iter().map(|c| Eye::from_char(c)).collect();

        assert!(top_iter.is_ok());
        assert!(bottom_iter.is_ok());

        let trigrams = build_trigrams(top_iter.unwrap().into_iter(), bottom_iter.unwrap().into_iter());

        assert!(trigrams.is_ok());
        let base10_values: Vec<u8> = trigrams.unwrap().into_iter()
            .map(|trigram| u8::from(trigram))
            .collect();
        assert_eq!(base10_values, vec![50, 66]);
    }
}
