use std::str::FromStr;
use std::vec::IntoIter;
use std::convert::{TryFrom, TryInto};

#[derive(Debug, Clone, Eq, PartialEq)]
enum EyeDirection {
    Center,
    Up,
    Right,
    Down,
    Left,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Eye {
    direction: EyeDirection,
}

#[derive(Debug, Clone)]
pub struct EyeMessage(Vec<Vec<Eye>>);

impl Eye {
    pub fn from_char(c: char) -> Result<Self, String> {
        let string = c.to_string();
        let value: u8 = string.parse().expect("Cannot parse value as u8");
        value.try_into()
    }
}

impl From<Eye> for u8 {
    fn from(eye: Eye) -> Self {
        match eye.direction {
            EyeDirection::Center => 0,
            EyeDirection::Up => 1,
            EyeDirection::Right => 2,
            EyeDirection::Down => 3,
            EyeDirection::Left => 4,
        }
    }
}

impl TryFrom<u8> for Eye {
    type Error = String;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Eye { direction: EyeDirection::Center }),
            1 => Ok(Eye { direction: EyeDirection::Up }),
            2 => Ok(Eye { direction: EyeDirection::Right }),
            3 => Ok(Eye { direction: EyeDirection::Down }),
            4 => Ok(Eye { direction: EyeDirection::Left }),
            _ => Err("Invalid input".to_string())
        }
    }
}

impl FromStr for EyeMessage {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut eyes = vec![];

        for eyes_line in s.lines() {
            eyes.push(eyes_line.chars().into_iter()
                .filter_map(|c| match c {
                    '\n' => None,
                    '5' => None,
                    _ => Some(Eye::from_char(c))
                })
                .collect::<Result<Vec<Eye>, String>>()?)
        }

        Ok(EyeMessage(eyes))
    }
}

impl IntoIterator for EyeMessage {
    type Item = Vec<Eye>;
    type IntoIter = IntoIter<Vec<Eye>>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}
